#include <stdlib.h>

struct cell_t {
  void* val;
  unsigned long int id;//taille de la liste
  struct cell_t* next;
};

typedef struct cell_t* list_t;

//renvoie une liste vide
list_t list_empty(){
  return NULL;
}

//verifie si la liste est vide
int list_is_empty(list_t l){
  return l==NULL;
}

//
list_t list_push(list_t l, void* x){
  list_t ret = malloc(sizeof(struct cell_t));
  if(list_is_empty(l))
    ret->id = 1;
  else
    ret->id = l->id + 1;
  ret->val = x;
  ret->next = l;
  return ret;
}

//l=(x:xs)

//renvoie la queue xs de la liste
list_t list_tail(list_t l){
  return l->next;
}

//renvoie x et modifie l tel que l=xs
void* list_pop(list_t* l){
  void* ret = (*l)->val;
  list_t tmp = (*l)->next;
  free(*l);
  *l = tmp;
  return ret;
}

//renvoie x
void* list_top(list_t l){
  return l->val;
}

//detruire l
void list_destroy(list_t l, void (*free_void)(void*)){
  if(!list_is_empty(l))
    free_void(list_pop(&l));
}

// return the found element or NULL
//recherche l'element x
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
  while(!list_is_empty(l)){
    if(l->val == x)
      return l;
    l=l->next;
    }
  return NULL;
}

//
unsigned long int list_len(list_t l){
  if(list_is_empty(l))
    return 0;
  else
    return l->id;
}
